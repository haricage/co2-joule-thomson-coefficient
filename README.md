# CO2 Joule-Thomson Coefficient Prediction

This is a readme file for swpco2jt.jl. Code written by Hariharan Ramachandran.<br>

<!--The documentation is available in the page  https://haricage.gitlab.io/co2-density-prediction -->

**Usage**<br>
This [Julia](https://julialang.org/) code calculates the Joule-Thomson Coefficient of pure CO2 based on the [Span-Wagner Technical Equation Of State](https://link.springer.com/article/10.1023/A:1022390430888) (Multiparameter EOS Formulation). 

Run swpco2jt in the runtime environment. In the Julia Repl window, type --> swjt(Pressure in MPa, Temperature in K). The Joule-Thomson Coefficient output will be in K/MPa. The first term in the output is the gas phase and the second term is the liquid phase.<br>

Example --> swjt(7.0,300.0)<br>
Output --> (0.0, 2.18879866671097)<br>

