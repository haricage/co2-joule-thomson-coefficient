module m
    # critical terms and values for CO2
    tc = 304.1282;              # critical temperature in K
    pc = 7.3773;                # critical pressure in MPa
    rhoceos = 446.62;           # critical density in kg/m3
    rhoc = 467.6;               # critical density in kg/m3
    rmol = 8.314510;            # molar gas constant in j/mol/k
    molwt = 44.0098;            # molecular weight in g/mol
    r = rmol/molwt;             # specific gas constant in j/g/K
    # n constants
    n1 = 0.89875108; n2 = -2.1281985; n3 = -0.068190320; n4 = 0.076355306;
    n5 = 0.00022053253; n6 = 0.41541823; n7 = 0.71335657; n8 = 0.00030354234;
    n9 = -0.36643143; n10 = -0.0014407781; n11 = -0.089166707; n12 = -0.023699887;
    # d constants
    d1 = 1.00; d2 = 1.00; d3 = 1.00; d4 = 3.00; d5 = 7.00; d6 = 1.00;
    d7 = 2.00; d8 = 5.00; d9 = 1.00; d10 = 1.00; d11 = 4.00; d12 = 2.00;
    # t constants
    t1 = 0.25; t2 = 1.25; t3 = 1.50; t4 = 0.25; t5 = 0.875; t6 = 2.375; t7 = 2.00;
    t8 = 2.125; t9 = 3.50; t10 = 6.50; t11 = 4.75; t12 = 12.50;
    # p constants
    p6 = 1.0; p7 = 1.0; p8 = 1.0; p9 = 2.0; p10 = 2.0; p11 = 2.0; p12 = 3.0;
    # a constants
    a1 = 8.37304456; a2 = -3.70454304; a3 = 2.5; a4 = 1.99427042;
    a5 = 0.62105248; a6 = 0.41195293; a7 = 1.04028922; a8 = 0.08327678;
    # theta constants
    th4 = 3.15163; th5 = 6.1119; th6 = 6.77708; th7 = 11.32384; th8 = 27.08792;
end
#--------------------------------------------------------------------------
# All the subsequent functions are calculated based on equations from span
# wagner book/ papers (reduced form and full form)
    # Function to calculate alpha residual
    function alfr(tau,del)
        alphar1 = m.n1*del^m.d1*tau^m.t1+m.n2*del^m.d2*tau^m.t2+m.n3*del^m.d3*tau^m.t3+m.n4*del^m.d4*tau^m.t4+m.n5*del^m.d5*tau^m.t5;
        alphar2 = m.n6*del^m.d6*tau^m.t6*exp(-del^m.p6)+m.n7*del^m.d7*tau^m.t7*exp(-del^m.p7);
        alphar3 = m.n8*del^m.d8*tau^m.t8*exp(-del^m.p8)+m.n9*del^m.d9*tau^m.t9*exp(-del^m.p9)+m.n10*del^m.d10*tau^m.t10*exp(-del^m.p10);
        alphar4 = m.n11*del^m.d11*tau^m.t11*exp(-del^m.p11)+m.n12*del^m.d12*tau^m.t12*exp(-del^m.p12);
        alphar = alphar1+alphar2+alphar3+alphar4;
        return alphar
    end
#--------------------------------------------------------------------------
    # Function to calculate differential of alpha residual w.r.t delta
    function dalfrdel(tau,del)
        alphar1 = m.n1*m.d1*del^(m.d1-1.0)*tau^m.t1+m.n2*m.d2*del^(m.d2-1.0)*tau^m.t2+m.n3*m.d3*del^(m.d3-1.0)*tau^m.t3;
        alphar2 = m.n4*m.d4*del^(m.d4-1.0)*tau^m.t4+m.n5*m.d5*del^(m.d5-1.0)*tau^m.t5;
        alphar3 = m.n6*del^(m.d6-1.0)*(m.d6-m.p6*del^m.p6)*tau^m.t6*exp(-del^m.p6)+m.n7*del^(m.d7-1.0)*(m.d7-m.p7*del^m.p7)*tau^m.t7*exp(-del^m.p7);
        alphar4 = m.n8*del^(m.d8-1.0)*(m.d8-m.p8*del^m.p8)*tau^m.t8*exp(-del^m.p8)+m.n9*del^(m.d9-1.0)*(m.d9-m.p9*del^m.p9)*tau^m.t9*exp(-del^m.p9);
        alphar5 = m.n10*del^(m.d10-1.0)*(m.d10-m.p10*del^m.p10)*tau^m.t10*exp(-del^m.p10)+m.n11*del^(m.d11-1.0)*(m.d11-m.p11*del^m.p11)*tau^m.t11*exp(-del^m.p11);
        alphar6 = m.n12*del^(m.d12-1.0)*(m.d12-m.p12*del^m.p12)*tau^m.t12*exp(-del^m.p12);
        alphar = alphar1+alphar2+alphar3+alphar4+alphar5+alphar6;
        return alphar
    end
#--------------------------------------------------------------------------
    # Function to calculate  double differential of alpha residual w.r.t delta
    function d2alfrdel(tau,del)
        alphar1 = m.n1*m.d1*(m.d1-1.0)*del^(m.d1-2.0)*tau^m.t1+m.n2*m.d2*(m.d2-1.0)*del^(m.d2-2.0)*tau^m.t2+m.n3*m.d3*(m.d3-1.0)*del^(m.d3-2.0)*tau^m.t3;
        alphar2 = m.n4*m.d4*(m.d4-1.0)*del^(m.d4-2.0)*tau^m.t4+m.n5*m.d5*(m.d5-1.0)*del^(m.d5-2.0)*tau^m.t5;
        alphar6 = m.n6*del^(m.d6-2.0)*((m.d6-m.p6*del^m.p6)*(m.d6-1.0-m.p6*del^m.p6)-m.p6^2.0*del^m.p6)*tau^m.t6*exp(-del^m.p6);
        alphar7 = m.n7*del^(m.d7-2.0)*((m.d7-m.p7*del^m.p7)*(m.d7-1.0-m.p7*del^m.p7)-m.p7^2.0*del^m.p7)*tau^m.t7*exp(-del^m.p7);
        alphar8 = m.n8*del^(m.d8-2.0)*((m.d8-m.p8*del^m.p8)*(m.d8-1.0-m.p8*del^m.p8)-m.p8^2.0*del^m.p8)*tau^m.t8*exp(-del^m.p8);
        alphar9 = m.n9*del^(m.d9-2.0)*((m.d9-m.p9*del^m.p9)*(m.d9-1.0-m.p9*del^m.p9)-m.p9^2.0*del^m.p9)*tau^m.t9*exp(-del^m.p9);
        alphar10 = m.n10*del^(m.d10-2.0)*((m.d10-m.p10*del^m.p10)*(m.d10-1.0-m.p10*del^m.p10)-m.p10^2.0*del^m.p10)*tau^m.t10*exp(-del^m.p10);
        alphar11 = m.n11*del^(m.d11-2.0)*((m.d11-m.p11*del^m.p11)*(m.d11-1.0-m.p11*del^m.p11)-m.p11^2.0*del^m.p11)*tau^m.t11*exp(-del^m.p11);
        alphar12 = m.n12*del^(m.d12-2.0)*((m.d12-m.p12*del^m.p12)*(m.d12-1.0-m.p12*del^m.p12)-m.p12^2.0*del^m.p12)*tau^m.t12*exp(-del^m.p12);
        alphar = alphar1+alphar2+alphar6+alphar7+alphar8+alphar9+alphar10+alphar11+alphar12;
        return alphar;
    end
#--------------------------------------------------------------------------
    # Function to calculate differential of alpha residual w.r.t tau
    function dalfrtau(tau,del)
        alphar1 = m.n1*m.t1*del^m.d1*tau^(m.t1-1.0)+m.n2*m.t2*del^m.d2*tau^(m.t2-1.0)+m.n3*m.t3*del^m.d3*tau^(m.t3-1.0);
        alphar2 = m.n4*m.t4*del^m.d4*tau^(m.t4-1.0)+m.n5*m.t5*del^m.d5*tau^(m.t5-1.0);
        alphar3 = m.n6*m.t6*del^m.d6*tau^(m.t6-1.0)*exp(-del^m.p6)+m.n7*m.t7*del^m.d7*tau^(m.t7-1.0)*exp(-del^m.p7);
        alphar4 = m.n8*m.t8*del^m.d8*tau^(m.t8-1.0)*exp(-del^m.p8)+m.n9*m.t9*del^m.d9*tau^(m.t9-1.0)*exp(-del^m.p9);
        alphar5 = m.n10*m.t10*del^m.d10*tau^(m.t10-1.0)*exp(-del^m.p10)+m.n11*m.t11*del^m.d11*tau^(m.t11-1.0)*exp(-del^m.p11);
        alphar6 = m.n12*m.t12*del^m.d12*tau^(m.t12-1.0)*exp(-del^m.p12);
        alphar = alphar1+alphar2+alphar3+alphar4+alphar5+alphar6;
        return alphar;
    end
#--------------------------------------------------------------------------
    # Function to calculate double differential of alpha residual w.r.t tau
    function d2alfrtau(tau,del)
        alphar1 = m.n1*m.t1*(m.t1-1.0)*del^m.d1*tau^(m.t1-2.0)+m.n2*m.t2*(m.t2-1.0)*del^m.d2*tau^(m.t2-2.0)+m.n3*m.t3*(m.t3-1.0)*del^m.d3*tau^(m.t3-2.0);
        alphar2 = m.n4*m.t4*(m.t4-1.0)*del^m.d4*tau^(m.t4-2.0)+m.n5*m.t5*(m.t5-1.0)*del^m.d5*tau^(m.t5-2.0);
        alphar3 = m.n6*m.t6*(m.t6-1.0)*del^m.d6*tau^(m.t6-2.0)*exp(-del^m.p6)+m.n7*m.t7*(m.t7-1.0)*del^m.d7*tau^(m.t7-2.0)*exp(-del^m.p7);
        alphar4 = m.n8*m.t8*(m.t8-1.0)*del^m.d8*tau^(m.t8-2.0)*exp(-del^m.p8)+m.n9*m.t9*(m.t9-1.0)*del^m.d9*tau^(m.t9-2.0)*exp(-del^m.p9);
        alphar5 = m.n10*m.t10*(m.t10-1.0)*del^m.d10*tau^(m.t10-2.0)*exp(-del^m.p10)+m.n11*m.t11*(m.t11-1.0)*del^m.d11*tau^(m.t11-2.0)*exp(-del^m.p11);
        alphar6 = m.n12*m.t12*(m.t12-1.0)*del^m.d12*tau^(m.t12-2.0)*exp(-del^m.p12);
        alphar = alphar1+alphar2+alphar3+alphar4+alphar5+alphar6;
        return alphar;
    end
#--------------------------------------------------------------------------
    # Function to calculate double differential of alpha residual w.r.t tau and delta
    function d2alfrdeltau(tau,del)
        alphar1 = m.n1*m.d1*m.t1*del^(m.d1-1.0)*tau^(m.t1-1.0)+m.n2*m.d2*m.t2*del^(m.d2-1.0)*tau^(m.t2-1.0)+m.n3*m.d3*m.t3*del^(m.d3-1.0)*tau^(m.t3-1.0);
        alphar2 = m.n4*m.d4*m.t4*del^(m.d4-1.0)*tau^(m.t4-1.0)+m.n5*m.d5*m.t5*del^(m.d5-1.0)*tau^(m.t5-1.0);
        alphar3 = m.n6*m.t6*del^(m.d6-1.0)*(m.d6-m.p6*del^m.p6)*tau^(m.t6-1.0)*exp(-del^m.p6)+m.n7*m.t7*del^(m.d7-1.0)*(m.d7-m.p7*del^m.p7)*tau^(m.t7-1.0)*exp(-del^m.p7);
        alphar4 = m.n8*m.t8*del^(m.d8-1.0)*(m.d8-m.p8*del^m.p8)*tau^(m.t8-1.0)*exp(-del^m.p8)+m.n9*m.t9*del^(m.d9-1.0)*(m.d9-m.p9*del^m.p9)*tau^(m.t9-1.0)*exp(-del^m.p9);
        alphar5 = m.n10*m.t10*del^(m.d10-1.0)*(m.d10-m.p10*del^m.p10)*tau^(m.t10-1.0)*exp(-del^m.p10)+m.n11*m.t11*del^(m.d11-1.0)*(m.d11-m.p11*del^m.p11)*tau^(m.t11-1.0)*exp(-del^m.p11);
        alphar6 = m.n12*m.t12*del^(m.d12-1.0)*(m.d12-m.p12*del^m.p12)*tau^(m.t12-1.0)*exp(-del^m.p12)
        alphar = alphar1+alphar2+alphar3+alphar4+alphar5+alphar6;
        return alphar;
    end
#--------------------------------------------------------------------------
    # Function to calculate standard state alphao
    function alfo(tau,del)
        alphao1 = log(del)+m.a1+m.a2*tau+m.a3*log(tau);
        alphao2 = m.a4*log(1.0-exp(-tau*m.th4))+m.a5*log(1.0-exp(-tau*m.th5))+m.a6*log(1.0-exp(-tau*m.th6));
        alphao3 = m.a7*log(1.0-exp(-tau*m.th7))+m.a8*log(1.0-exp(-tau*m.th8));
        alphao = alphao1+alphao2+alphao2;
        return alphao
    end
#--------------------------------------------------------------------------
    # Function to calculate differential of alphao w.r.t tau

    function dalfotau(tau,del)
        alphao1 = m.a2+m.a3/tau;
        alphao2 = m.a4*m.th4*(1.0/(1.0-exp(-tau*m.th4))-1.0)+m.a5*m.th5*(1.0/(1.0-exp(-tau*m.th5))-1.0)+m.a6*m.th6*(1.0/(1.0-exp(-tau*m.th6))-1.0);
        alphao3 = m.a7*m.th7*(1.0/(1.0-exp(-tau*m.th7))-1.0)+m.a8*m.th8*(1.0/(1.0-exp(-tau*m.th8))-1.0);
        alphao = alphao1+alphao2+alphao3;
        return alphao;
    end

#--------------------------------------------------------------------------
    # Function to calculate double differential of alphao w.r.t tau
    function d2alfotau(tau,del)
        alphao1 = -m.a3/tau^2.0-m.a4*m.th4^2.0*exp(-tau*m.th4)*(1.0/(1.0-exp(-tau*m.th4))^2.0);
        alphao2 = m.a5*m.th5^2.0*exp(-tau*m.th5)*(1.0/(1.0-exp(-tau*m.th5))^2.0)+m.a6*m.th6^2.0*exp(-tau*m.th6)*(1.0/(1.0-exp(-tau*m.th6))^2.0);
        alphao3 = m.a7*m.th7^2.0*exp(-tau*m.th7)*(1.0/(1.0-exp(-tau*m.th7))^2.0)+m.a8*m.th8^2.0*exp(-tau*m.th8)*(1.0/(1.0-exp(-tau*m.th8))^2.0);
        alphao = alphao1-alphao2-alphao3;
        return alphao;
    end
#--------------------------------------------------------------------------
    # Function to calculate differential of alphao w.r.t delta
    function dalfodel(tau,del)
        alphao = 1.0/del;
        return alphao;
    end
#--------------------------------------------------------------------------
    # Function to calculate double differential of alphao w.r.t delta
    function d2alfodel(tau,del)
        alphao = -1.0/del^2.0;
        return alphao;
    end
#--------------------------------------------------------------------------
# Function to solve for density
# Newton-Raphson algorithm over del. Will generate two different
# results based on initial guesses.
function densol(p,t,dguess)
    count2 = 0;
    maxval = 100;
    dold = dguess;
    dnew = 0.1;
    tau = m.tc/t;
    tol = 1.0e-7;
    fold = (1000.0*p/m.rhoc/dold/m.r/t)-1.0-dold*dalfrdel(tau,dold);
    fdash = -(1000.0*p/m.rhoc/dold^2.0/m.r/t)-dalfrdel(tau,dold)-dold*d2alfrdel(tau,dold);
    for i in 1:maxval
        if dold < 0.0 || dold > 3.0
            if dguess < 1.0
                dnew = 0.4;
                count2 = count2 +1;
            else
                dnew = 1.3;
                count2 = count2 +1;
            end
        else
            dnew = dold-fold/fdash;
        end
        fnew = (1000.0*p/m.rhoc/dnew/m.r/t)-1.0-dnew*dalfrdel(tau,dnew);
        fdash = -(1000.0*p/m.rhoc/dnew^2.0/m.r/t)-dalfrdel(tau,dnew)-dnew*d2alfrdel(tau,dnew);
        if abs(fnew) < tol
            break
        else
            dold = dnew;
            fold = fnew;
        end
        if i == maxval || count2 == 2
            dnew = 10.0;
            break
        end
    end
    del = dnew;
    fugacity = exp(alfr(tau,del)+del*dalfrdel(tau,del)-log(1.0+del*dalfrdel(tau,del)));
    den = del*m.rhoc;
    return den, fugacity;
end
#--------------------------------------------------------------------------
# Phase equilibrium solver
function swdenco2(p,t)
    (deng, fugg) = densol(p,t,0.001);
    (denl, fugl) = densol(p,t,2.0);
    # Check if the densities are the same
    errd::Float64 = abs(deng-denl)::Float64;
    if errd < 1.0e-3
        if deng <= m.rhoceos
            fugl = 10000000.0;
        else
            fugg = 10000000.0;
        end
    end
    errf::Float64 = abs(fugg-fugl);
    if errf < 1.0e-7
        deng = deng;
        denl = denl;
    elseif fugg < fugl
        deng = deng;
        denl = 0.0;
    else
        deng = 0.0;
        denl = denl;
    end
    #den::Tuple{deng::Float64,::Float64};
    den = (deng,denl);      # density is in kg/m3
    #println("density = $den")
    return den;
end
#--------------------------------------------------------------------------
#Fugacity calculator
function swfugco2(p,t)
    den = swdenco2(p,t)
    tau = m.tc/t;
    del = den./m.rhoc;
    fugacity = zeros(2);
    for i in 1:2
        if del[i] == 0.0
            fugacity[i] = 0.0;
        else
            fugacity[i] = exp(alfr(tau,del[i])+del[i]*dalfrdel(tau,del[i])-log(1.0+del[i]*dalfrdel(tau,del[i])));
        end
    end
    return fugacity
end
#--------------------------------------------------------------------------
#joule-thomson coefficient
function swjt(p,t)
    den = swdenco2(p,t);
    tau = m.tc/t;
    del = den./m.rhoc;
    jt = zeros(2);
    for i in 1:2
        if del[i] == 0.0
            jt[i] = 0.0;
        else
            jt[i] = -1.005*1000.0*(1.0/m.r/den[i])*(del[i]*dalfrdel(tau,del[i])+del[i]^2.0*d2alfrdel(tau,del[i])+tau*del[i]*d2alfrdeltau(tau,del[i]))*((1.0+del[i]*dalfrdel(tau,del[i])-tau*del[i]*d2alfrdeltau(tau,del[i]))^2.0-tau^2.0*(d2alfotau(tau,del[i])+d2alfrtau(tau,del[i]))*(1.0+2.0*del[i]*dalfrdel(tau,del[i])+del[i]^2.0*d2alfrdel(tau,del[i])))^-1.0;
        end
    end
    jtout = (jt[1], jt[2]); # joule-thomson coefficient is in K/MPa
    return jtout
end
#--------------------------------------------------------------------------